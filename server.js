var Students = [];

const fastify = require('fastify')({ logger: true })

fastify.get('/report', function(req,res){
	
	if(Students){
		res.send(Students)	
	}
	else{
		res.send("Something Went Wrong")
	}
})

fastify.post('/add', function(req, res) {
    var Student = req.body;
    console.log(Student);
    const found = Students.some(id => id.StudentID === Student.StudentID);
    if (!found){
    	Students.push(Student);
    	res.send("Student Added!");
    }
    else{
    	res.send("Student already exist");
    }

});

fastify.delete('/delete', async(req, res) => {

	var data = req.body

	for(let i = 0; i < Students.length; i++){
		if(Students[i].StudentID == data.StudentID){
			id = Students[i].Student_Id;
			Students.splice(i,1);
			break;
		}
	}
	res.send("Record Removed")
})

fastify.post('/update', async(req,res) => {
	
	var data = req.body

	for(let i = 0; i < Students.length; i++){
		if(Students[i].StudentID == data.StudentID){
				for(eachSubject in data){
					Students[i][eachSubject] = data[eachSubject]
				}
		}
	}
	res.send("Record updated")
})

fastify.listen(3000, function(err){
	if(err){
		fastify.log.error(err)
		process.exit(1)
	}
})
